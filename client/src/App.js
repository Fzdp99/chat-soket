import "./App.css";
import Chat from "./chat";
import { Routes, Route } from "react-router-dom";

const link = [
  {
    path: "/",
    component: <Chat />,
  },
];

function App() {
  return (
    <div className="App">
      <Routes>
        {link.map((data, i) => (
          <Route key={i} path={data.path} element={data.component}></Route>
        ))}
      </Routes>
    </div>
  );
}

export default App;
