import { useEffect, useState } from "react";
import io from "socket.io-client";
import "./App.css";

const socket = io.connect(process.env.REACT_APP_BACKEND_URL);

function App() {
  // room & name (login)
  const [room, setRoom] = useState("");
  const [name, setName] = useState("");
  const [lg, setLg] = useState(false);

  // msg
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  const login = () => {
    setLg(true);
    socket.emit("join room", room);
  };

  function handleTextChange(e) {
    setMessage(e.target.value);
    console.log(message);
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (!message || message === "") return;
    console.log("Submitted!");

    socket.emit("chat message", { message, room, name });
  }

  useEffect(() => {
    socket.on("incoming message", (data) => {
      setMessages([...messages, data]);
    });
  }, [socket, messages]);

  return (
    <div className="App">
      {!lg ? (
        <form onSubmit={login}>
          <div className="mb-3">
            <label for="exampleFormControlInput1" className="form-label">
              Name
            </label>
            <input
              onChange={(e) => setName(e.target.value)}
              type="text"
              className="form-control"
              id="exampleFormControlInput1"
            />
          </div>
          <div className="mb-3">
            <label for="exampleFormControlInput1" className="form-label">
              Room
            </label>
            <input
              onChange={(e) => setRoom(e.target.value)}
              type="text"
              className="form-control"
              id="exampleFormControlInput1"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Join room
          </button>
        </form>
      ) : (
        <div className="kotakChat">
          <div className="App-messages">
            {messages.map((dt, index) => (
              <div className="App-message mss" key={index}>
                <p>From : {dt.name}</p>
                <p>Msg: {dt.message}</p>
              </div>
            ))}
          </div>
          <form className="App-control" onSubmit={handleSubmit}>
            <input
              type="text"
              placeholder="Message..."
              onChange={handleTextChange}
            />
            <input className="App-button" type="submit" value="Send" />
          </form>
        </div>
      )}
    </div>
  );
}

export default App;
